
import { Component, OnInit,Input,AfterViewInit,ViewChild } from '@angular/core';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import {Router} from '@angular/router';
import { SwiperComponent, SwiperDirective, SwiperConfigInterface,
  SwiperScrollbarInterface, SwiperPaginationInterface } from 'ngx-swiper-wrapper';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  
})
export class HomeComponent implements OnInit,AfterViewInit {
  items: Array<any> = [];
  breadcrumbname:string;
  categoryArry:Array<any> = [];
  categor:Array<any> = [];
  categorChild:Array<any> = [];
  categorRoom:Array<any> = [];
  public show: boolean = true;
  public slides = [
    {image:'http://brooklynmedia.co.ke/wp-content/uploads/2012/01/Business-1.jpg'},
    {image:'http://brooklynmedia.co.ke/wp-content/uploads/2012/01/Business-1.jpg'}
    
  ];
  public type: string = 'component';

  public disabled: boolean = false;

  public config: SwiperConfigInterface = {
    direction: 'horizontal',
    slidesPerView: 1,
    keyboard: true,
    mousewheel: false,
    scrollbar: false,
    navigation: true,
    pagination: false
  };

  private scrollbar: SwiperScrollbarInterface = {
    el: '.swiper-scrollbar',
    hide: false,
    draggable: true
  };

  private pagination: SwiperPaginationInterface = {
    el: '.swiper-pagination',
    clickable: true,
    hideOnClick: false
  };
  @ViewChild(SwiperComponent) componentRef?: SwiperComponent;
  @ViewChild(SwiperDirective) directiveRef?: SwiperDirective;
  

  constructor(private router: Router, private _sanitizer : DomSanitizer,
    ) { 
    this.items = [
      { name: 'assets/cover image.png' },
      { name: 'assets/Coverimage.jpg' },
      { name: 'assets/cover image.png' },
      { name: 'assets/Coverimage.jpg' }
      
    ]
    this.categor=['2','1','4','34','45','64','6'];
    this.categorChild=['2','1','4','34','45','64','6'];
    this.categorRoom=['2','1','4','34','45','64','6'];
  }
     
  ngOnInit() {
    this.categoryArry=JSON.parse(sessionStorage.getItem('category'))

  }
  ngAfterViewInit(){
    //this.categoryArry=sessionStorage.getItem('category')
    console.log(this.categoryArry);
  }
  
  onNewEventClick(){
    console.log('home'); 
    this.router.navigateByUrl('/occasion');
  }
   public pageNumber : number = 0;
    public pageCount : number = 0;
    public pages = [
    {image: "assets/coverimage.png",text:'A healthy outside starts from the inside'},
        {image: "assets/coverimage.png",text:'Skip the diet, just eat healthy'},
        {image: "assets/coverimage.png",text:'You are What You Eat'},
        
    ];

    getBackground(image) {
      return this._sanitizer.bypassSecurityTrustStyle(`url(${image})`);
    }

    onJuiceClick(e){
      //console.log(breadcrumbname);
      console.log(e.target.sizes);
    
       
      console.log(e.target.title)
      sessionStorage.setItem('breadcrumbname',e.target.title)
      this.router.navigateByUrl('/category');
    }
    onSandwicheClick(e){
      //this.breadcrumbname=true;
      console.log(e.target.title)
      sessionStorage.setItem('breadcrumbname',e.target.title)
      this.router.navigateByUrl('/sandwiches');
    }
    onEggOrderClick(e){
      //this.breadcrumbname=true;
      console.log(e.target.title)
      sessionStorage.setItem('breadcrumbname',e.target.title)
      this.router.navigateByUrl('/eggsorder');
    }
    onSideEatsClick(e){
      //this.breadcrumbname=true;
      console.log(e.target.title)
      sessionStorage.setItem('breadcrumbname',e.target.title)
      this.router.navigateByUrl('/sideeat');
    }
    onBiryanisClick(e){
      //this.breadcrumbname=true;
      console.log(e.target.title)
      sessionStorage.setItem('breadcrumbname',e.target.title)
      this.router.navigateByUrl('/biryani');
    }
    onSaladsClick(e){
      //this.breadcrumbname=true;
      console.log(e.target.title)
      sessionStorage.setItem('breadcrumbname',e.target.title)
      this.router.navigateByUrl('/salad');
    }
    onSteaksClick(e){
      //this.breadcrumbname=true;
      console.log(e.target.title)
      sessionStorage.setItem('breadcrumbname',e.target.title)
      this.router.navigateByUrl('/steaks');
    }
    onSmoothiesClick(e){
      //this.breadcrumbname=true;
      console.log(e.target.title)
      sessionStorage.setItem('breadcrumbname',e.target.title)
      this.router.navigateByUrl('/smoothies');
    }
    public toggleType(): void {
    this.type = (this.type === 'component') ? 'directive' : 'component';
  }

  public toggleDisabled(): void {
    this.disabled = !this.disabled;
  }

  public toggleDirection(): void {
    this.config.direction = (this.config.direction === 'horizontal') ? 'vertical' : 'horizontal';
  }

  public toggleSlidesPerView(): void {
    if (this.config.slidesPerView !== 1) {
      this.config.slidesPerView = 1;
    } else {
      this.config.slidesPerView = 2;
    }
  }

  public toggleOverlayControls(): void {
    if (this.config.navigation) {
      this.config.scrollbar = false;
      this.config.navigation = false;

      this.config.pagination = this.pagination;
    } else if (this.config.pagination) {
      this.config.navigation = false;
      this.config.pagination = false;

      this.config.scrollbar = this.scrollbar;
    } else {
      this.config.scrollbar = false;
      this.config.pagination = false;

      this.config.navigation = true;
    }

    if (this.type === 'directive' && this.directiveRef) {
      this.directiveRef.setIndex(0);
    } else if (this.type === 'component' && this.componentRef && this.componentRef.directiveRef) {
      this.componentRef.directiveRef.setIndex(0);
    }
  }

  public toggleKeyboardControl(): void {
    this.config.keyboard = !this.config.keyboard;
  }

  public toggleMouseWheelControl(): void {
    this.config.mousewheel = !this.config.mousewheel;
  }

  public onIndexChange(index: number): void {
    console.log('Swiper index: ', index);
  }

  public onSwiperEvent(event: string): void {
    console.log('Swiper event: ', event);
  }



}
