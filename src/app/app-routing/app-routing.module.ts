import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from '../main/main.component';
import { HomeComponent } from '../home/home.component';



const routes: Routes = [
      {
        path: '',
        component: MainComponent,
        children: [
        {
            path: 'home',
            component: HomeComponent,
        }
        ]
        
    },

];

@NgModule({
    imports: [
       RouterModule.forRoot(routes),
    ],
    exports: [
        RouterModule
    ],
    declarations: []
})
export class AppRoutingModule { }
